# Juju Client

The Juju API client CLI in a container for CI automation.

## Usage

```yaml
juju:
  image: registry.gitlab.com/painless-software/docker/juju:3.5.0
  script:
  - juju deploy ./application.charm
```

## Tags

- version tag ... stable images with Juju version `major.minor.patch`
- `latest` ... latest stable image (tagged with a Juju version)
- `main` ... edge image built from the `main` branch
- `mr<IID>` ... review images built from merge requests, which are deleted
   once a MR is merged or closed

See the repository's [container registry][registry] for all image tags.

## Development

See [CONTRIBUTING][contrib] for instructions on development and testing.

## Related Resources

- https://juju.is/docs/olm/install-juju
- https://launchpad.net/juju/

[registry]: https://gitlab.com/painless-software/docker/juju/container_registry
[contrib]: CONTRIBUTING.md

# Install juju from binary. https://launchpad.net/juju/
# https://juju.is/docs/olm/install-juju#heading--install-juju-from-binary

ARG DOCKER_REGISTRY=docker.io

FROM ${DOCKER_REGISTRY}/library/alpine:latest

ARG VERSION
ARG PATCH
ARG RELEASE=${VERSION}.${PATCH}
ARG ARCHIVE=juju-${RELEASE}-linux-amd64.tar.xz
ARG DOWNLOAD_URL=https://launchpad.net/juju/${VERSION}/${RELEASE}/+download/${ARCHIVE}
ARG USER=juju
ARG HOME=/home/${USER}

WORKDIR /tmp

RUN apk add --no-cache curl \
 && curl -sLO ${DOWNLOAD_URL} \
 && curl -sL ${DOWNLOAD_URL}/+md5 -o juju.md5 \
 && apk del curl \
 && cat juju.md5 | md5sum -c \
 && tar xf ${ARCHIVE} \
 && install -o root -g root -m 0755 juju /usr/local/bin/juju \
 && rm -v juju* \
 && adduser -D -u 1001 ${USER} ${USER} \
# Support arbitrary user IDs (OpenShift guidelines)
 && chown -R ${USER}:0 ${HOME} \
 && chmod -R g=u       ${HOME}

WORKDIR ${HOME}

USER ${USER}

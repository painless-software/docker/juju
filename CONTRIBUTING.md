# Contributing

You're welcome to contribute updates, bug fixes or other enhancements!

## Development

1. Look up current latest version number from [Launchpad][lp].
1. Update `VERSION` and `PATCH` level in the [CI configuration][version].
1. Commit and push changes to a topic branch and open a MR to build a
   review image.
1. Wait for the MR to be merged, which will build the *edge* image based
   on the `main` branch.
1. To create a *stable* image [create a release with a Git tag][releases]
   matching the Juju version.

The CI/CD pipeline will automatically build container images with
various tags as described in the [README][tags]. See the repository's
[container registry][registry] for all current image tags.

## Tests

[Functional tests and vulnerability checks][tests] run in the CI/CD
pipeline's `test` stage using the freshly built container image.
When you add features or fix a bug, please add or update a related test.

While a MR is active you'll find an image tagged `mr<IID>`, the merge
request ID, in the [container registry][registry] for your convenience,
which you can use to manually test-drive your changes.

[lp]: https://launchpad.net/juju/
[releases]: https://gitlab.com/painless-software/docker/juju/-/releases
[registry]: https://gitlab.com/painless-software/docker/juju/container_registry
[version]: .gitlab-ci.yml#L3-4
[tests]: .gitlab-ci.yml#L97-127
[tags]: README.md#tags
